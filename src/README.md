# Sicherheit

Zur Absicherung der Kommunikation und der Datenkonsistenz gibt es verschiedene Ansätze. Einige dieser Ansätze liste ich unten mit Beschreibungen auf.

## Zertifikat

Hierbei werden jeweils ein Client- und ein Serverzertifikat erstellt, über die dann die Kommunikation verschlüsselt wird. Dies hat den Vorteil, dass die Datenmenge weiterhin gering bleibt.

## API Key / Secret Key

Hierbei wird in der Kommunikation ein sogenannter API-Key mitgegeben, der serverseitig geprüft wird. Falls der API-Key nicht korrekt ist, kann die Kommunikation abgebrochen werden. Diese Methode ist zwar einfach, hat jedoch den Nachteil, dass die Kommunikation zuerst akzeptiert wird und anschließend erst abgebrochen wird. Außerdem werden mehr Daten übertragen.

## CRC-Prüfung

Hierbei würde von den vorherigen Daten ein CRC-Hash mitgegeben werden. Falls dieser mit den Daten der vorherigen Kommunikation übereinstimmt, werden die Daten als korrekt abgespeichert. Auch hier fallen mehr Daten in der Kommunikation an.