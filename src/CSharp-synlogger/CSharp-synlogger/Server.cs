﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Configuration;
using System.Net;
using System.Net.Sockets;

namespace CSharp_synlogger
{
    class Server
    {
        public Server()
        {
            int port = int.Parse(ConfigurationManager.AppSettings["Port"]);
            int bufferSize = int.Parse(ConfigurationManager.AppSettings["BufferSize"]);

            TcpListener server = new TcpListener(IPAddress.Any, port);
            server.Start();

            while (true)
            {
                TcpClient client = server.AcceptTcpClient();

                NetworkStream ns = client.GetStream();

                while (client.Connected)
                {
                    byte[] msgBuffer = new byte[bufferSize];
                    ns.Read(msgBuffer, 0, bufferSize);

                    int size = 0;
                    for (; size < msgBuffer.Length; size++)
                    {
                        if (msgBuffer[size] == 0)
                            break;
                    }

                    Array.Resize(ref msgBuffer, size);

                    string csv = System.Text.Encoding.UTF8.GetString(msgBuffer);
                    if (csv.Contains("\n"))
                    {
                        try
                        {
                            string deviceId = csv.Substring(0, csv.IndexOf('\n'));
                            csv = csv.Substring(csv.IndexOf('\n') + 1);

                            string jsonStr = CsvToJson(csv);
                            new Files().writeDataInFile(deviceId, jsonStr);
                        }
                        catch (Exception exc)
                        {
                            Console.WriteLine(exc.StackTrace);
                        }
                    }

                    client.Close();
                }
            }
        }

        private string CsvToJson(string csvStr)
        {

            ArrayList arrayData = new ArrayList();

            foreach (string row in csvStr.Split('\n'))
            {
                if (row.Length > 0)
                {
                    ArrayList tmp = new ArrayList();
                    bool firstColum = true;
                    foreach (string colum in row.Split(','))
                    {
                        if (firstColum)
                        {
                            tmp.Add(MyDateTime.ConvertMsToDateTimeString(long.Parse(colum)));
                            firstColum = false;
                        }
                        else
                        {
                            tmp.Add(colum);
                        }
                    }
                    arrayData.Add(tmp);
                }
            }
            string jsonStr = JsonConvert.SerializeObject(arrayData);
            return jsonStr;
        }
    }
}
