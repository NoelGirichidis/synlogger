﻿using System;

namespace CSharp_synlogger
{
    class MyDateTime
    {
        public static string GetCurrentDateTime()
        {
            DateTime now = DateTime.Now;
            string myDateTime = now.ToString("yyyyMMddHmmssfffffff");

            return myDateTime;
        }

        public static string ConvertMsToDateTimeString(long msDateTime)
        {
            try
            {
                DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeMilliseconds(msDateTime / 1_000);
                string myDateTimeStr = dateTimeOffset.ToString("yyyy-MM-ddTH:mm:ss.fffffff");
                return myDateTimeStr;
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.StackTrace);
                return null;
            }
        }
    }
}
