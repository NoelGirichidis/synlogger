﻿using System;
using System.Configuration;
using System.IO;

namespace CSharp_synlogger
{
    class Files
    {
        public void writeDataInFile(string deviceId, string data)
        {
            string path = ConfigurationManager.AppSettings["Path"];
            checkPath(path);
            string pathFileName = path + deviceId + "-" + MyDateTime.GetCurrentDateTime() + ".json";
            StreamWriter sw = new StreamWriter(pathFileName, true);
            sw.Write(data);
            sw.Close();
        }

        private void checkPath(string path)
        {
            try
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.StackTrace);
            }
        }
    }
}
