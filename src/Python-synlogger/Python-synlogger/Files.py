import os
import MyDateTime


def createPath(path):
  if not os.path.exists(path):
    os.makedirs(path)

def checkFile(filename):
    if(not os.path.isfile(filename)):
        file = open(filename, "w+")
        return file
    else:
        file = open(filename, "a+")
        return file

def WriteDataInFile(path, data):
    createPath(path)
    file = checkFile(path + data['DeviceId'] + "-" + MyDateTime.GetCurrentDateTime() +".json");
    file.write(data['Data']);
    file.close();