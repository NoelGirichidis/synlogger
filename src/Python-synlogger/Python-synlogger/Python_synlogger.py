import socket
import sys
import json

import Files
import MyDateTime

_PATH = "../../../Data/";
_PORT = 8412;
_ADDRESS = "localhost";


def csvToJson(csvData):
    deviceId = csvData.partition('\n')[0];
    csvData = csvData.split("\n",2)[2]
    arrayData = [];
    for row in csvData.split("\n"):
        if(len(row)>0):
            tmp = [];
            firstColum = True;
            for colum in row.split(","):
                if(firstColum):
                    tmp.append(MyDateTime.ConvertMsToDateTimeString(int(colum)));
                    firstColum = False;
                else:
                    tmp.append(colum);
            arrayData.append(tmp);
    json_string = json.dumps(arrayData);
    return {'DeviceId':deviceId, 'Data':json_string};



if __name__ == '__main__':
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM);
    server_address = (_ADDRESS, _PORT);

    sock.bind(server_address);
    sock.listen(1);

    while True:
        connection, client_address = sock.accept();
        csvData = "";
        try:

            while True:
                data = connection.recv(16)
                csvData += str(data, encoding='utf-8');
                if data:
                    print("");
                else:
                    break
            
        finally:
            connection.close();

        if (csvData.find('\n')!=1):
            data = csvToJson(csvData);
            Files.WriteDataInFile(_PATH, data);
