from datetime import datetime, timedelta

def GetCurrentDateTime():
	current_dateTime = datetime.now();
	currentDateTimeStr = current_dateTime.strftime("%Y%m%d%H%M%S%f")
	return currentDateTimeStr;

def ConvertMsToDateTimeString(msDateTime):
	my_datetime = datetime.fromtimestamp(msDateTime / 1_000_000)
	myDateTimeStr =  my_datetime.strftime("%Y-%m-%dT%H:%M:%S.%f");
	return myDateTimeStr;
