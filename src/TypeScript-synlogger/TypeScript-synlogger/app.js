Object.defineProperty(exports, "__esModule", { value: true });
const DateTime_1 = require("./DateTime");
var net = require('net');
var fs = require('fs');
var dt = require('./DateTime');


var PATH = "../../../Data/";
var PORT = 8412;


var server = net.createServer();


server.on('close', function () {
    console.log('Server closed !');
});

server.on('connection', function (socket) {
    socket.setEncoding('utf8');
    socket.on('data', function (data) {
        let bread = socket.bytesRead;
        let bwrite = socket.bytesWritten;
        console.log('Bytes read : ' + bread);
        console.log('Bytes written : ' + bwrite);
        var deviceId = data.split('\n')[0];
        data = data.substring(data.indexOf('\n'));
        var arrayData = [];
        data.split('\n').forEach(function (row) {
            if (row.length > 0) {
                var tmp = [];
                var firstColum = true;
                row.split(',').forEach(function (colum) {
                    if (firstColum) {
                        var dateTimeStr = UnixEpochToDatTime(colum);
                        tmp.push(dateTimeStr);
                        firstColum = false;
                    }
                    else {
                        tmp.push(colum);
                    }
                });
                arrayData.push(tmp);
            }
        });
        var json_string = JSON.stringify(arrayData);
        WriteDataInFile({ 'DeviceId': deviceId, 'Data': json_string });
    });
    socket.on('close', function (error) {
        let bread = socket.bytesRead;
        let bwrite = socket.bytesWritten;
        console.log('Bytes read : ' + bread);
        console.log('Bytes written : ' + bwrite);
        console.log('Socket closed!');
        if (error) {
            console.log('Socket was closed coz of transmission error');
        }
    });
});

server.listen(PORT, '127.0.0.1');


function UnixEpochToDatTime(msDateTime) {
    let date = new Date(msDateTime / 1000);
    return date.toISOString();
}

function GetCurrentDateTime() {
    let datetime = new DateTime_1.DateTime(new Date()).dateToDateTimeString(2);
    return datetime;
}

function WriteDataInFile(data) {
    fs.writeFileSync(PATH + data['DeviceId'] + '-' + GetCurrentDateTime() + '.json', data['Data']);
}