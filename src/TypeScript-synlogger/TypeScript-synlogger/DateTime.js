Object.defineProperty(exports, "__esModule", { value: true });

exports.DateTime = void 0;

class DateTime {
    constructor(datetimeraw, d, M, y, h, m, s, ms) {
        this.datetimeraw = datetimeraw;
        this.d = d;
        this.M = M;
        this.y = y;
        this.h = h;
        this.m = m;
        this.s = s;
        this.ms = ms;
        if (datetimeraw != undefined) {
            this.d = datetimeraw.getDate();
            this.M = datetimeraw.getMonth();
            this.y = datetimeraw.getFullYear();
            this.h = datetimeraw.getHours();
            this.m = datetimeraw.getMinutes();
            this.s = datetimeraw.getSeconds();
            this.ms = datetimeraw.getMilliseconds();
        }
        this.M += 1;
    }
    /**
     * Gibt ein formatierten Datumsstring zur�ck
     * @param i 0: 'ddMMyyyy'
     * @param i 1: 'dd-MM-yyyy'
     * @param i 2: 'ddMMyyyy_hhmmssfff'
     * @param i 3: 'dd.MM.yyyy_hh:mm:ss.fff'
     * @param i 4: 'dd.MM.yyyy hh:mm:ss.fff'
     */
    dateToDateTimeString(i) {
        let str;
        switch (i) {
            case 0:
                str = (this.d <= 9 ? '0' : '')
                    + this.d + (this.M <= 9 ? '0' : '') + this.M
                    + this.y;
                break;
            case 1:
                str = (this.d <= 9 ? '0' : '') + this.d + '-'
                    + (this.M <= 9 ? '0' : '') + this.M + '-'
                    + this.y;
                break;
            case 2:
                str = (this.d <= 9 ? '0' : '') + this.d
                    + (this.M <= 9 ? '0' : '') + this.M
                    + this.y + '_'
                    + (this.h <= 9 ? '0' : '') + this.h
                    + (this.m <= 9 ? '0' : '') + this.m
                    + (this.s <= 9 ? '0' : '') + this.s
                    + (this.ms <= 9 ? '00' : (this.ms <= 99 ? '0' : '')) + this.ms;
                break;
            case 3:
                str = (this.d <= 9 ? '0' : '') + this.d + '.'
                    + (this.M <= 9 ? '0' : '') + this.M + '.'
                    + this.y + '_'
                    + (this.h <= 9 ? '0' : '') + this.h + ':'
                    + (this.m <= 9 ? '0' : '') + this.m + ':'
                    + (this.s <= 9 ? '0' : '') + this.s + '.'
                    + (this.ms <= 9 ? '00' : (this.ms <= 99 ? '0' : '')) + this.ms;
                break;
            case 4:
                str = (this.d <= 9 ? '0' : '') + this.d + '.'
                    + (this.M <= 9 ? '0' : '') + this.M + '.'
                    + this.y + ' '
                    + (this.h <= 9 ? '0' : '') + this.h + ':'
                    + (this.m <= 9 ? '0' : '') + this.m + ':'
                    + (this.s <= 9 ? '0' : '') + this.s + '.'
                    + (this.ms <= 9 ? '00' : (this.ms <= 99 ? '0' : '')) + this.ms;
                break;
            default:
                break;
        }
        return str;
    }
}
exports.DateTime = DateTime;