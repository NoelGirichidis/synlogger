#include "main.hpp"


 
int server(){
    int serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (serverSocket == -1) {
        std::cerr << "Fehler beim Erstellen des Sockets." << std::endl;
        return 1;
    }

    sockaddr_in serverAddress;
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    serverAddress.sin_port = htons(PORT);

    if (bind(serverSocket, (struct sockaddr*)&serverAddress, sizeof(serverAddress)) == -1) {
        std::cerr << "Fehler beim Binden des Sockets." << std::endl;
        close(serverSocket);
        return 1;
    }

    if (listen(serverSocket, 5) == -1) {
        std::cerr << "Fehler beim Warten auf Verbindungen." << std::endl;
        close(serverSocket);
        return 1;
    }

    std::cout << "Server wartet auf Verbindungen..." << std::endl;

    while (true) {
        int clientSocket = accept(serverSocket, nullptr, nullptr);
        if (clientSocket == -1) {
            std::cerr << "Fehler beim Akzeptieren der Verbindung." << std::endl;
            close(serverSocket);
            return 1;
        }
        
        std::cout << "Client verbunden." << std::endl;
        
        char data[BUFFERSIZE];
        int currentPos = 0;
        
        size_t bytesRead;
        while (true) {
            char buffer[BUFFERSIZE];
            bytesRead = recv(clientSocket, buffer, sizeof(buffer), 0);

            if (bytesRead <= 0) {
                close(clientSocket);
                break;
            }

            for (ssize_t i = 0; i < bytesRead; ++i) {
                data[currentPos] = buffer[i];
                currentPos++;
            }
        }
        std::string deviceId = GetDeviceId(data);
        std::string jsonStr = ConvertToJson(data);
        
        std::string filename = deviceId + '-' + GetCurrentDateTime() + ".json";
        
        WriteInFile(PATH, filename, jsonStr);
        
        std::cout << jsonStr;
        std::cout << std::endl;
    }
    
    close(serverSocket);
    return 1;
}
int main() {
    
    int ret = server();
    
    return ret;
}
