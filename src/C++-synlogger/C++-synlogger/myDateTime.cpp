#include <iostream>
#include <cstring>
#include <unistd.h>

#include "myDateTime.hpp"

std::string ConvertEpochToTime(long long epochMicroseconds) {
    char dateTime[100];
    
    std::chrono::seconds secondsSinceEpoch(epochMicroseconds / 1000000);
    std::chrono::microseconds microseconds(secondsSinceEpoch);
    microseconds = std::chrono::microseconds(epochMicroseconds - microseconds.count() * 1000000);

    std::chrono::system_clock::time_point timePoint = std::chrono::system_clock::time_point(secondsSinceEpoch) + microseconds;

    std::time_t time = std::chrono::system_clock::to_time_t(timePoint);
    std::tm tmInfo = *std::localtime(&time);

    // Formatiere die Zeit als Zeichenkette (z.B., "YYYY-MM-DD HH:MM:SS.uuuuuu")
    std::strftime(dateTime, sizeof(dateTime), "%Y-%m-%d %H:%M:%S", &tmInfo);
    int micros = static_cast<int>(microseconds.count());
    snprintf(dateTime + strlen(dateTime), sizeof(dateTime) - strlen(dateTime), ".%06d", micros);

    return dateTime;
}


std::string GetCurrentDateTime(){
    char dateTime[100];
    std::chrono::system_clock::time_point now = std::chrono::system_clock::now();

    // Konvertieren Sie time_point in ein std::tm-Objekt für das aktuelle Datum/Uhrzeit
    std::time_t currentTime = std::chrono::system_clock::to_time_t(now);
    std::tm tmInfo = *std::localtime(&currentTime);

    // Formatieren Sie das Datum/Uhrzeit als Zeichenkette (z.B., "YYYY-MM-DD HH:MM:SS")
    std::strftime(dateTime, sizeof(dateTime), "%Y%m%d%H%M%S", &tmInfo);

    return dateTime;
}
