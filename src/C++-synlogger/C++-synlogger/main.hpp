#ifndef main_h
#define main_h

#include <iostream>
#include <cstring>
#include <unistd.h>
#include <sstream>
#include <arpa/inet.h>
#include <sys/socket.h>

#include "myDateTime.hpp"
#include "myData.hpp"
#include "myFile.hpp"


#define PORT        8412
#define BUFFERSIZE  4096
#define PATH "/Users/noel/Desktop/data/"

#endif /* main_h */
