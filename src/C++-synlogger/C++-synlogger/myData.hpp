//
//  myData.hpp
//  C++-synlogger
//
//  Created by Noel Girichidis on 09.10.23.
//

#ifndef myData_hpp
#define myData_hpp

#include <stdio.h>
#include <iostream>
#include <cstring>
#include <unistd.h>
#include <sstream>

std::string GetDeviceId(std::string data);
std::string ConvertToJson(std::string data);

#endif /* myData_hpp */
