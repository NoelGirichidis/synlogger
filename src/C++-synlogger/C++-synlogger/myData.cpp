//
//  myData.cpp
//  C++-synlogger
//
//  Created by Noel Girichidis on 09.10.23.
//

#include "myData.hpp"

std::string GetDeviceId(std::string data){
    std::string deviceId = data.substr(0, data.find('\n'));
    return deviceId;
}

std::string ConvertToJson(std::string data){
    std::string csvData = data.substr(data.find('\n')+1, data.find('\0'));
    
    std::string csvNewData = "";
    std::string jsonStr = "[";
    while(true){
        try {
            int y = csvData.find('\n');
            if(y==-1)
                break;
            
            std::string row = csvData.substr(0, y);
            jsonStr += "[";
            int nextSub=0;
            bool firstColum = true;
            while(true){
                int x = row.find(',');
                std::string tmp = row.substr(0, x);
                /*
                 if(firstColum){
                    long long epoch;
                    std::istringstream ( tmp ) >> epoch;
                    tmp = ConvertEpochToTime(epoch);
                    firstColum = false;
                 }
                 */
                jsonStr += '"' + tmp + '"';
                row = row.substr(x+1);
                if(x==-1)
                    break;
                jsonStr += ",";
            }
            jsonStr += ']';
            csvData = csvData.substr(y+1);
            jsonStr += ',';
        } catch (...) {
            break;
        }
    }
    jsonStr = jsonStr.substr(0, jsonStr.find_last_of(','));
    jsonStr += "]";
    return jsonStr;
}
