//
//  main.cpp
//  C++-synlogger
//
//  Created by Noel Girichidis on 09.10.23.
//
/*
#include <iostream>
#include <cstring>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

int main(int argc, const char * argv[]) {

    int serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (serverSocket == -1) {
        std::cerr << "Fehler beim Erstellen des Sockets." << std::endl;
        return 1;
    }

    sockaddr_in serverAddress;
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    serverAddress.sin_port = htons(12345);

    if (bind(serverSocket, (struct sockaddr*)&serverAddress, sizeof(serverAddress)) == -1) {
        std::cerr << "Fehler beim Binden des Sockets." << std::endl;
        close(serverSocket);
        return 1;
    }

    if (listen(serverSocket, 5) == -1) {
        std::cerr << "Fehler beim Warten auf Verbindungen." << std::endl;
        close(serverSocket);
        return 1;
    }

    std::cout << "Server wartet auf Verbindungen..." << std::endl;

    while (true) {
        int clientSocket = accept(serverSocket, nullptr, nullptr);
        if (clientSocket == -1) {
            std::cerr << "Fehler beim Akzeptieren der Verbindung." << std::endl;
            close(serverSocket);
            return 1;
        }

        std::cout << "Client verbunden." << std::endl;

        char buffer[1024];
        ssize_t bytesRead = recv(clientSocket, buffer, sizeof(buffer), 0);

        if (bytesRead <= 0) {
            std::cerr << "Fehler beim Empfangen von Daten vom Client." << std::endl;
            close(clientSocket);
            continue;
        }

        std::cout << "Empfangene Daten vom Client: ";
        for (ssize_t i = 0; i < bytesRead; ++i) {
            std::cout << buffer[i];
        }
        std::cout << std::endl;

        close(clientSocket);
    }

    close(serverSocket);
 
    std::cout << "Hello, World!\n";
    return  0;
}



 
 
 
 
#include <iostream>
#include <cstring>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#define PORT        8412
#define BUFFERSIZE  4096

int main(int argc, const char * argv[]) {

    int serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (serverSocket == -1) {
        std::cerr << "Fehler beim Erstellen des Sockets." << std::endl;
        return 1;
    }

    // Konfiguriere die Server-Adresse
    sockaddr_in serverAddress;
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    serverAddress.sin_port = htons(PORT);

    // Binde den Socket an die Adresse
    if (bind(serverSocket, (struct sockaddr*)&serverAddress, sizeof(serverAddress)) == -1) {
        std::cerr << "Fehler beim Binden des Sockets." << std::endl;
        close(serverSocket);
        return 1;
    }

    // Warte auf eingehende Verbindungen
    if (listen(serverSocket, 5) == -1) {
        std::cerr << "Fehler beim Warten auf Verbindungen." << std::endl;
        close(serverSocket);
        return 1;
    }
    
    std::cout << "Server wartet auf Verbindungen..." << std::endl;

    while (true) {
        // Akzeptiere eine eingehende Verbindung
        int clientSocket = accept(serverSocket, nullptr, nullptr);
        if (clientSocket == -1) {
            std::cerr << "Fehler beim Akzeptieren der Verbindung." << std::endl;
            close(serverSocket);
            return 1;
        }

        std::cout << "Client verbunden." << std::endl;

        // Empfange Daten vom Client
        char buffer[BUFFERSIZE]; // Puffer für die empfangenen Daten
        int bytesRead = recv(clientSocket, buffer, BUFFERSIZE, 0);
        if (bytesRead <= 0) {
            std::cerr << "Fehler beim Empfangen von Daten vom Client." << std::endl;
            close(clientSocket);
            continue;
        }

        // Ausgabe der empfangenen Daten auf der Konsole
        std::cout << "Empfangene Daten vom Client: " << buffer << std::endl;

        // Schließe die Verbindung zum Client
        close(clientSocket);
    }

    // Schließe den Server-Socket
    close(serverSocket);

//    return 0;
    
    std::cout << "Hello, World!\n";
    return 0;
}
 
 
 
 
 
 #include <iostream>
 #include <cstring>
 #include <unistd.h>
 #include <arpa/inet.h>
 #include <sys/socket.h>

 #define PORT 8412
 #define BUFFERSIZE 1024

 int main(int argc, const char * argv[]) {
     
      int serverSocket = socket(AF_INET, SOCK_STREAM, 0);
      if (serverSocket == -1) {
          std::cerr << "Fehler beim Erstellen des Sockets." << std::endl;
          return 1;
      }
      
      sockaddr_in serverAddress;
      serverAddress.sin_family = AF_INET;
      serverAddress.sin_addr.s_addr = INADDR_ANY;
      serverAddress.sin_port = htons(PORT);
      
      if (bind(serverSocket, (struct sockaddr*)&serverAddress, sizeof(serverAddress)) == -1) {
          std::cerr << "Fehler beim Binden des Sockets." << std::endl;
          close(serverSocket);
          return 1;
      }
      
      if (listen(serverSocket, 5) == -1) {
          std::cerr << "Fehler beim Warten auf Verbindungen." << std::endl;
          close(serverSocket);
          return 1;
      }
      
      std::cout << "Server wartet auf Verbindungen..." << std::endl;
      
      while (true) {
          int clientSocket = accept(serverSocket, nullptr, nullptr);
          if (clientSocket == -1) {
              std::cerr << "Fehler beim Akzeptieren der Verbindung." << std::endl;
              close(serverSocket);
              return 1;
          }
          
          std::cout << "Client verbunden." << std::endl;
          
          char buffer[BUFFERSIZE];
          ssize_t bytesRead = recv(clientSocket, buffer, sizeof(buffer), 0);
          
          
          
          if (bytesRead <= 0) {
              std::cerr << "Fehler beim Empfangen von Daten vom Client." << std::endl;
              close(clientSocket);
              continue;
          }
          
          std::cout << "Empfangene Daten vom Client: ";
          for (ssize_t i = 0; i < BUFFERSIZE; ++i) {
              std::cout << buffer[i];
          }
          std::cout << std::endl;
          
          close(clientSocket);
      }
      
      close(serverSocket);
      
     std::cout << "Hello, World!\n";
     return  0;
 }

*/
