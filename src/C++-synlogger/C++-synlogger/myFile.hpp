#ifndef myFile_hpp
#define myFile_hpp

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <filesystem>

namespace fs = std::filesystem;

void WriteInFile(std::string path, std::string filename, std::string data);
void CheckFilePath(std::string path);

#endif /* myFile_hpp */
