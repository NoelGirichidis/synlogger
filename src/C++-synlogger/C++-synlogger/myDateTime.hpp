#ifndef myDateTime_hpp
#define myDateTime_hpp

#include <stdio.h>
#include <iostream>
#include <cstring>
#include <unistd.h>

std::string ConvertEpochToTime(long long epochMicroseconds);

std::string GetCurrentDateTime();

#endif /* myDateTime_hpp */
