#include "myFile.hpp"

void WriteInFile(std::string path, std::string filename, std::string data) {
    CheckFilePath(path);

    std::ofstream myFile(path+filename);
    myFile << data;
    myFile.close();
}

void CheckFilePath(std::string path){
    if (!fs::is_directory(path) || !fs::exists(path)) {
        fs::create_directory(path);
    }
}
